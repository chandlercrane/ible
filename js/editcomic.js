// Get Edit Comic Book Button Element
const editComicButton = document.getElementById('editComicButton')
const TitleInput = document.getElementById('TitleInput')
const editPriceInput = document.getElementById('editPriceInput')
const editLocationInput = document.getElementById('editLocationInput')
const editQuantityInput = document.getElementById('editQuantityInput')

// Store the ID of the comic we're editing rn
var prevId = 0

// Clear input fields...
function editComic(id){
	TitleInput.value = document.getElementById('titleid=' + id).innerText	
	editPriceInput.value = document.getElementById('price=' + id).innerText	
	editLocationInput.value = document.getElementById('loc=' + id).innerText	
	editQuantityInput.value = document.getElementById('qty=' + id).innerText	
	prevId = id
}

// Logic when button is pressed...
editComicButton.addEventListener('click', e => {
	url = "http://db.cse.nd.edu/cse30246/ible/projectfiles/editcomic.php?"
	price = editPriceInput.value
	loc = editLocationInput.value
	count = editQuantityInput.value

	url += "id=" + prevId + "&"
	url += "price=" + price + "&"
	url += "loc=" + loc + "&"
	url += "quantity=" + count

	window.location = url
}) 
