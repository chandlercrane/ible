// Get Add Comic Book Button Element
const addComicButton = document.getElementById('addComicButton')
const addTitleInput = document.getElementById('addTitleInput')
const addPriceInput = document.getElementById('addPriceInput')
const addLocationInput = document.getElementById('addLocationInput')
const addComicPopupButton = document.getElementById('addComicPopupButton')
const addQuantityInput = document.getElementById('addQuantityInput')

// Clear input fields...
addComicPopupButton.addEventListener('click', e => {
	addTitleInput.value = ""
	addPriceInput.value = ""
	addLocationInput.value = ""
	addQuantityInput.value = ""
})

// Logic when button is pressed...
addComicButton.addEventListener('click', e => {
	url = "http://db.cse.nd.edu/cse30246/ible/projectfiles/brick.php?"
	title = addTitleInput.value
	price = addPriceInput.value
	loc = addLocationInput.value
	count = addQuantityInput.value

	url += "id=" + title + "&"
	url += "price=" + price + "&"
	url += "loc=" + loc + "&"
	url += "quantity=" + count

	window.location = url
})
