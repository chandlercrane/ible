// Get Add Card Book Button Element
const addIdInput = document.getElementById('addIDInput')
const addQtyInput = document.getElementById('addQtyInput')
const addCardButton = document.getElementById('addCardButton')
const addCardPopupButton = document.getElementById('addCardPopupButton')

// Clear input fields...
addCardPopupButton.addEventListener('click', e => {
	addIdInput.value = ""
	addQtyInput.value = ""
})

// Logic when button is pressed...
addCardButton.addEventListener('click', e => {
	url = "http://db.cse.nd.edu/cse30246/ible/projectfiles/insert_card.php?"
	title = addIdInput.value
	count = addQtyInput.value

	url += "id=" + title + "&"
	url += "quantity=" + count

	window.location = url
})
