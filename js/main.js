// Get DOM elements
const logoutButton = document.getElementById('logoutButton')

// Add logout event
logoutButton.addEventListener('click', e => {
	// logout logic
	window.location = './logout.php'	
})

const updatePasswordButton = document.getElementById('updatePasswordButton')
const passwordInput = document.getElementById('passwordInput')
// Add change password event
updatePasswordButton.addEventListener('click', e => {
	var newPassword = passwordInput.value
	window.location.replace("./user_page.php?password=" + newPassword)
})

const usernameInput = document.getElementById('usernameInput')
const sessionUsername = document.getElementById('sessionUsername')

const nameLabel = document.getElementById('nameLabel')

// Show username in field
function getUsername(){
	usernameInput.value = "Balls"
	usernameInput.value = sessionUsername.innerText
	nameLabel.innerText = sessionUsername.innerText
}

getUsername()

const deleteUserButton = document.getElementById('deleteUserButton')
// Add delete user event
deleteUserButton.addEventListener('click', e => {
	window.location.replace("http://db.cse.nd.edu/cse30246/ible/projectfiles/delete_user.php?")
})

// Add section changing code
const dashboardButton = document.getElementById('dashboardButton')
const settingsButton = document.getElementById('settingsButton')
const inventoryButton = document.getElementById('inventoryButton')
const cardButton = document.getElementById('cardButton')

const dashboardSection = document.getElementById('dashboardSection')
const settingsSection = document.getElementById('settingsSection')
const inventorySection = document.getElementById('inventorySection')
const cardSection = document.getElementById('cardSection')

// Section button switching logic
dashboardButton.addEventListener('click', e => {
    dashboardSection.style = "display:block"
    dashboardButton.classList.remove("sidebar-button-color-notselected")
    dashboardButton.classList.add("sidebar-button-color-selected")
    settingsSection.style = "display:none"
    settingsButton.classList.remove("sidebar-button-color-selected")
    settingsButton.classList.add("sidebar-button-color-notselected")
    inventorySection.style = "display:none"
    inventoryButton.classList.remove("sidebar-button-color-selected")
  	inventoryButton.classList.add("sidebar-button-color-notselected")
    cardSection.style = "display:none"
    cardButton.classList.remove("sidebar-button-color-selected")
  	cardButton.classList.add("sidebar-button-color-notselected")
	setParam('px', 0)
})
inventoryButton.addEventListener('click', e => {
    dashboardSection.style = "display:none"
    dashboardButton.classList.remove("sidebar-button-color-selected")
    dashboardButton.classList.add("sidebar-button-color-notselected")
    settingsSection.style = "display:none"
    settingsButton.classList.remove("sidebar-button-color-selected")
    settingsButton.classList.add("sidebar-button-color-notselected")
    inventorySection.style = "display:block"
    inventoryButton.classList.remove("sidebar-button-color-notselected")
    inventoryButton.classList.add("sidebar-button-color-selected")
    cardSection.style = "display:none"
    cardButton.classList.remove("sidebar-button-color-selected")
  	cardButton.classList.add("sidebar-button-color-notselected")
	setParam('px', 1)
})
cardButton.addEventListener('click', e => {
    dashboardSection.style = "display:none"
    dashboardButton.classList.remove("sidebar-button-color-selected")
    dashboardButton.classList.add("sidebar-button-color-notselected")
    settingsSection.style = "display:none"
    settingsButton.classList.remove("sidebar-button-color-selected")
    settingsButton.classList.add("sidebar-button-color-notselected")
    inventorySection.style = "display:none"
    inventoryButton.classList.remove("sidebar-button-color-selected")
  	inventoryButton.classList.add("sidebar-button-color-notselected")
    cardSection.style = "display:block"
    cardButton.classList.remove("sidebar-button-color-notselected")
  	cardButton.classList.add("sidebar-button-color-selected")
	setParam('px', 2)	
})
settingsButton.addEventListener('click', e => {
    dashboardSection.style = "display:none"
    dashboardButton.classList.remove("sidebar-button-color-selected")
    dashboardButton.classList.add("sidebar-button-color-notselected")
    settingsSection.style = "display:block"
    settingsButton.classList.remove("sidebar-button-color-notselected")
    settingsButton.classList.add("sidebar-button-color-selected")
    inventorySection.style = "display:none"
    inventoryButton.classList.remove("sidebar-button-color-selected")
  	inventoryButton.classList.add("sidebar-button-color-notselected")
    cardSection.style = "display:none"
    cardButton.classList.remove("sidebar-button-color-selected")
  	cardButton.classList.add("sidebar-button-color-notselected")
	setParam('px', 3)	
})

const clearComicSearchButton = document.getElementById('clearComicSearchButton')
clearComicSearchButton.addEventListener('click', e => {
	setParam('search', '')
	window.location.href = window.location.href
})
const clearCardSearchButton = document.getElementById('clearCardSearchButton')
clearCardSearchButton.addEventListener('click', e => {
	setParam('search2', '')
	window.location.href = window.location.href
})

function setParam(param, value){
	var url = window.location.search
	var searchParams = new URLSearchParams(url)
	searchParams.set(param, value)

	if (history.pushState) {
		var newurl = window.location.origin + window.location.pathname + "?" + searchParams.toString()
		window.history.pushState({path:newurl},'',newurl);
	}
}

function getUrlParameter(name){
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(location.search);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
}

function showpage(px){
	if(px == 0){
		dashboardSection.style = "display:block"
		dashboardButton.classList.remove("sidebar-button-color-notselected")
		dashboardButton.classList.add("sidebar-button-color-selected")
	}
	else if(px == 1){
		inventorySection.style = "display:block"
		inventoryButton.classList.remove("sidebar-button-color-notselected")
		inventoryButton.classList.add("sidebar-button-color-selected")
	}
	else if(px == 2){
		cardSection.style = "display:block"
		cardButton.classList.remove("sidebar-button-color-notselected")
	  cardButton.classList.add("sidebar-button-color-selected")
	}
	else if(px == 3){
		settingsSection.style = "display:block"
		settingsButton.classList.remove("sidebar-button-color-notselected")
		settingsButton.classList.add("sidebar-button-color-selected")
	}
} 

var urlParams = new URLSearchParams(window.location.search);
var pageIndex = getUrlParameter('px')

showpage(pageIndex)

