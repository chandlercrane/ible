<?php
session_start(); 
$link = mysqli_connect('localhost', 'ebianchi', 'ebianchi') or die ('bleh');
mysqli_select_db($link, 'ebianchi');

$username = $_SESSION['user_id'];
date_default_timezone_set('America/New_York');
$date = date("Y-m-d");

$sql = "select username, cast(sum(price*quantity) as DECIMAL(10,2)) from card_inventories,ownership where card_inventories.card_id = ownership.card_id and ownership.username = ? and day = '" . $date . "' group by username;";
$stmt = $link->prepare($sql);
$stmt->bind_param('s', $_SESSION['user_id']);
$stmt->execute();
$stmt->bind_result($user, $total);

echo "<div class=\"dataValues\">";

while($stmt->fetch()){
	echo "<h5 class=\"totalValue main-title-color\">Total Value: $$total</h5>";
}
echo "<br>";
$sql = "select username, cast(sum(price*quantity)/sum(quantity) as DECIMAL(10,2)) from card_inventories,ownership where card_inventories.card_id = ownership.card_id and username = ? and day = '" . $date . "' group by username;";
$stmt = $link->prepare($sql);
$stmt->bind_param('s', $_SESSION['user_id']);
$stmt->execute();
$stmt->bind_result($user, $avg);

while($stmt->fetch()){
	echo "<h5 class=\"avgValue main-title-color\">Average Price: $$avg</h5>";
}

echo "</div>";

?>
