<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
?>
<?php
$target_dir = "/var/www/html/cse30246/ible/projectfiles/uploads/";
$target_file = $target_dir . basename($_FILES["fileToUploadComic"]["name"]);
$uploadOk = 1;
$fileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

// Check if file already exists
if (file_exists($target_file)) {
    echo "Sorry, file already exists.";
    $uploadOk = 0;
}
// Check file size
if ($_FILES["fileToUpload"]["size"] > 500000) {
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
}
// Allow certain file formats
if($fileType != "csv") {
    echo "Sorry, only CSV files are allowed.";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["fileToUploadComic"]["tmp_name"], $target_file)) {
        echo "The file ". basename( $_FILES["fileToUploadComic"]["name"]). " has been uploaded.";
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}

echo $target_file;

$link = mysqli_connect('localhost', 'ebianchi', 'ebianchi') or die ('failed to connect');
mysqli_select_db($link, 'ebianchi');

$sql = "load data local infile '" . $target_file . "' into table inventories fields terminated by ',' ignore 1 lines;";
if(mysqli_query($link, $sql)) {
	echo 'File upload to database';
} else {
	echo mysqli_error($link);
}

unlink($target_file);
header("Location: http://db.cse.nd.edu/cse30246/ible/projectfiles/home.php?px=1");

?>
