<?php
session_start(); 
$link = mysqli_connect('localhost', 'ebianchi', 'ebianchi') or die ('bleh');
mysqli_select_db($link, 'ebianchi');

$username = $_SESSION['user_id'];
date_default_timezone_set('America/New_York');
//$date = date("Y-m-d");
//echo $date;
$date = "2019-12-17";

if(isset($_GET['search2'])){
	$sql = "select card_inventories.card_id, card_title, price, quantity from ownership, card_inventories where card_inventories.card_id = ownership.card_id and day = ? and username = ? and card_title like ?";
	$stmt = $link->prepare($sql);
	$yo = '%' . $_GET['search2'] . '%';
	$stmt->bind_param('sss', $date, $_SESSION['user_id'], $yo);
	$stmt->execute();
	$stmt->bind_result($id, $title, $price, $qty);
} 
else {
		$sql = "select card_inventories.card_id, card_title, price, quantity from ownership, card_inventories where card_inventories.card_id = ownership.card_id and day = ? and username = ?";
		$stmt = $link->prepare($sql);
		$stmt->bind_param('ss', $date, $_SESSION['user_id']);
		$stmt->execute();
		$stmt->bind_result($id, $title, $price, $qty);
}

echo "<table class=\"table\" id=\"cardTable\">\n";
echo "<thead>";
	echo "<tr>";
		echo "<th scope=\"col\">ID</th>";
		echo "<th scope=\"col\">Title</th>";
		echo "<th scope=\"col\">Today's Price</th>";
		echo "<th scope=\"col\">Quantity</th>";
		echo "<th scope=\"col\">Add</th>";
		echo "<th scope=\"col\">Sub</th>";
		echo "<th scope=\"col\">Remove All</th>";
	echo "</tr>";
echo "</thead>";
echo "<tbody>";
while($stmt->fetch()){
	echo "\t<tr>\n";
	echo "\t<td>$id</td>\n";
	echo "\t<td id=\"tid=$id\">$title</td>\n";
	echo "\t<td id=\"p=$id\">$$price</td>\n";
	echo "\t<td id =\"q=$id\">$qty</td>\n";
	echo "\t<td><span class=\"table-remove\"><button type=\"button\" onclick=\"add_card($id)\" class=\"btn green-button-color shadow\">+</button></span></td>\n";
	echo "\t<td><span class=\"table-remove\"><button type=\"button\" onclick=\"sub_card($id)\" class=\"btn yellow-button-color shadow\">-</button></span></td>\n";
	echo "\t<td><span class=\"table-remove\"><button type=\"button\" onclick=\"remove_card($id)\" class=\"btn red-button-color shadow\">Remove</button></span></td>\n";
	echo "\t</tr>\n";
}
echo "</tbody>";
echo "</table>";

$stmt->close();

?>
