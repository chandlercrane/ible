
import random

def create_table(f):
	table = {}
	for line in f:
		found = 0
		index = line.find(',')
		first = line[:index]
		line = line[index + 1:]
		if line.find('"') != -1:
			found = 1
			end = line[1:].find('"')
			name = line[1:end+1]
			line = line[end + 3:]
		line = line.strip().split(',')
		line.insert(0, first)
		if found:
			line.insert(1, '"' + name + '"')
		name = line[1]
		table[name] = line

	return table


def make_new_day(table):
	f = open('fabrication.csv','w')
	for i in range(1,91):	
		for entry in table:
			temp = table[entry]
			day = int(temp[5].split('-')[2]) - i
			month = int(temp[5].split('-')[1])
			while day < 1:
				day += 30
				month -= 1
			if temp[4] != 'None':
				price = float(temp[4])
				price *= random.randint(10,16)/random.randint(3,12)
			f.write(','.join([temp[0], temp[1], temp[2], temp[3], str(round(price,2)), '-'.join(['2019',str(month),str(day)])]) +'\n')
			 

if __name__ == '__main__':
	
	f = open('cardPrices.csv','r')
	table = create_table(f)	
	make_new_day(table)
