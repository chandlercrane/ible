
<html>

	<head>
		<title>Login Here</title>	
		<link rel="stylesheet" href="./css/bootstrap.css">
        <link rel="stylesheet" href="./css/ible.css">
	</head>
	
	<body>
		</br>
		<div class="container">
			<?php
				if(!empty($_GET['error'])){
					if (strcmp($_GET['error'], "1") == 0){
						echo'<div class="alert alert-danger" role="alert">Incorrect Password</div>';
					}
					else if (strcmp($_GET['error'], "2") == 0){
						echo'<div class="alert alert-warning" role="alert">Username does not exist</div>';
					}
				}
				else{
					echo'<div class="alert alert-primary" role="alert">Please login below</div>';
				}
			?>
			<form action='validate.php' method='get' onsubmit="return validateUser();">
                <div class="form-group">
                    <label for="usernameLabel">Username</label>
                    <input required name="username" type="username" class="form-control" id="usernameField" placeholder="Username">
                </div>
                <div class="form-group">
                    <label for="passwordLabel">Password</label>
                    <input required name="password" type="password" class="form-control" id="passwordField" placeholder="Password">
                </div>
                <button type="submit" id="loginButton" class="btn btn-primary">
                    Login
                </button>
            </form>
			
			<a href="./register.php">Don't have an account? Register Here</a>

		</div>
		
		<script src="./js/login.js"></script>

	</body>

</html>
