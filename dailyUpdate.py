#!/escnfs/home/jcolli19/my_python/bin/python3.6

import requests
import json
import mysql.connector
from mysql.connector.constants import ClientFlag
from datetime import date
from twilio.rest import Client

HEADERS = {"Authorization": "Bearer RhiVkP4CJ747KtY8ChIT4uIQ-6Akrzzz8v0swcExHhosypsVfMr3Mull0pQjqBvSjYQiPWGHA7UqyqXs9Y095DWYxdnvSPa8HqP4K05ucKFp1mG0ldt0AVrOu-QE0YvZw_1UP0NLKZ_f0JE_k7XAK5rrsFghlDQ3h4fxNa48lz4sY77GHEfrQmN0_mng48W18SS58-8PlG7HbQHVKbFzoDquikjVqGSxCLkTy_bSFCPcmkBSEPuiHED9yZL5zFQSx9LWXQvCwOodzWk6Oq5z5tqPbWiWUfNcBenedxVDbNTRh5VMQaIaP4_-qdcvIF9LaUNLBw", "Content-Type": "application/json"}

def create_set_dict():
	
	setDict = {}
	offset = 0
	flag = 1
	
	while flag:
		
		url = 'http://api.tcgplayer.com/v1.32.0/catalog/groups'
		querystring = {"categoryId": "1", "limit":"20", "offset": str(offset)}
		response = requests.request("GET", url, params=querystring, headers=HEADERS)
		data = json.loads(response.text)

		if len(data['results']) == 0:
			flag = 0

		for result in data['results']:
			setDict[result['abbreviation']] = result['groupId']
		offset += 20

	return setDict

SETS = create_set_dict()

def get_current_inventory():
	cardInfo = {}

	mydb = mysql.connector.connect(
		host 	= "localhost",
		user 	= "ebianchi",
		passwd 	= "ebianchi",
		database= "ebianchi",
	)

	mycursor = mydb.cursor()

	mycursor.execute("select distinct card_title, card_id, set_name, type from card_inventories")

	myresult = mycursor.fetchall()

	for x in myresult:
		cardInfo[x[0]] = x[1:]

	return cardInfo

def get_product_ids(cardInfo):

	findProductUrl = 'http://api.tcgplayer.com/v1.32.0/catalog/products'
	ids = {}
	
	for name in cardInfo:
		try:
			querystring = {"categoryId":"1", "productName": name, "includeSkus":"false", "productTypes":["Cards"]}
			productResponse = requests.request("GET", findProductUrl, headers=HEADERS, params=querystring)
			productInfo = json.loads(productResponse.text)
			for result in productInfo['results']:
				prodId = result['productId']
				if SETS[cardInfo[name][1]] == result['groupId']:
					ids[str(prodId)] = [name, cardInfo[name][0], cardInfo[name][1], cardInfo[name][2]]
		except Exception as e:
			print(e)
	
	return ids


def get_prices(ids, cardInfo):
	n = 250
	listKeys = list(ids.keys())
	batches = [ listKeys[ i * n :(i+1) * n ] for i in range((len(listKeys)+ n -1) // n) ]
	c = open('/var/www/html/cse30246/ible/projectfiles/updatePrices.csv','w')
	for batch in batches:
		ids_url = '%2c'.join(batch)
		pricing_url = 'http://api.tcgplayer.com/v1.32.0/pricing/product/' + ids_url
		response = requests.request("GET", pricing_url, headers=HEADERS)
		try:
			for result in json.loads(response.text)['results']:
				prodId = str(result['productId'])
				if result['subTypeName'] == ids[prodId][3]:
					if ids[prodId][0].find(',') != -1:
						ids[prodId][0] = '"' + ids[prodId][0] + '"'
					c.write(','.join([str(ids[prodId][1]), ids[prodId][0], ids[prodId][2], ids[prodId][3], str(result['marketPrice']), str(date.today())]) + '\n')
		except Exception as e:
			print(e)

def update_database():

	mydb = mysql.connector.connect(
		host 	= "localhost",
		user 	= "ebianchi",
		passwd 	= "ebianchi",
		database= "ebianchi",
		allow_local_infile = "True"
	)

	mycursor = mydb.cursor()

	mycursor.execute("load data local infile '/var/www/html/cse30246/ible/projectfiles/updatePrices.csv' into table card_inventories fields terminated by ',' optionally enclosed by '\"' lines terminated by '\n' ")
	mydb.commit()

def build_message(user):
	
	message = ""

	mydb = mysql.connector.connect(
		host 	= "localhost",
		user 	= "ebianchi",
		passwd 	= "ebianchi",
		database= "ebianchi",
	)

	mycursor = mydb.cursor()
	
	date_ = str(date.today())
	year, month, day = date_.split('-')
	day = int(day)
	month = int(month)	

	d = day - 7
	m = month
	while d < 1:
		d += 30
		m -= 1
		if m < 1:
			m += 12 
	
	query = "select c.card_title, ((c.price/i.price)-1)*100 as percentage from card_inventories as c, card_inventories as i, ownership where c.card_id = i.card_id and c.day = '" + str(date.today()) + "' and i.day = '" + '-'.join([year,str(m),str(d)]) + "' and c.price != 0 and i.price != 0 and c.card_id = ownership.card_id and ownership.username = '" +user+ "' order by percentage limit 5"
	mycursor.execute(query)	
	myresult = mycursor.fetchall()
	message += "Top 5 Depreciations (Over 7 days): \n"
	for result in myresult:
		message += result[0] + ': ' + str(round(float(result[1]), 2)) + '%\n'
	message += '\n'	

	query = "select c.card_title, ((c.price/i.price)-1)*100 as percentage from card_inventories as c, card_inventories as i, ownership where c.card_id = i.card_id and c.day = '" + str(date.today()) + "' and i.day = '" + '-'.join([year,str(m),str(d)]) + "' and c.price != 0 and i.price != 0 and c.card_id = ownership.card_id and ownership.username = '" +user+ "' order by percentage desc limit 5"
	mycursor.execute(query)	
	myresult = mycursor.fetchall()
	message += "Top 5 Appreciations (Over 7 days): \n"
	for result in myresult:
		message += result[0] + ': ' + str(round(float(result[1]), 2)) + '%\n'
	message += '\n'	

	d = day - 30
	m = month
	while d < 1:
		d += 30
		m -= 1
		if m < 1:
			m += 12 
	query = "select c.card_title, ((c.price/i.price)-1)*100 as percentage from card_inventories as c, card_inventories as i, ownership where c.card_id = i.card_id and c.day = '" + str(date.today()) + "' and i.day = '" + '-'.join([year,str(m),str(d)]) + "' and c.price != 0 and i.price != 0 and c.card_id = ownership.card_id and ownership.username = '" +user+ "' order by percentage limit 5"
	mycursor.execute(query)	
	myresult = mycursor.fetchall()
	message += "Top 5 Depreciations (Over 30 days): \n"
	for result in myresult:
		message += result[0] + ': ' + str(round(float(result[1]), 2)) + '%\n'
	message += '\n'	

	query = "select c.card_title, ((c.price/i.price)-1)*100 as percentage from card_inventories as c, card_inventories as i, ownership where c.card_id = i.card_id and c.day = '" + str(date.today()) + "' and i.day = '" + '-'.join([year,str(m),str(d)]) + "' and c.price != 0 and i.price != 0 and c.card_id = ownership.card_id and ownership.username = '" +user+ "' order by percentage desc limit 5"
	mycursor.execute(query)	
	myresult = mycursor.fetchall()
	message += "Top 5 Appreciations (Over 30 days): \n"
	for result in myresult:
		message += result[0] + ': ' + str(round(float(result[1]), 2)) + '%\n'
	message += '\n'	

	d = day - 90
	m = month
	while d < 1:
		d += 30
		m -= 1
		if m < 1:
			m += 12 
	
	query = "select c.card_title, ((c.price/i.price)-1)*100 as percentage from card_inventories as c, card_inventories as i where c.card_id = i.card_id and c.day = '" + str(date.today()) + "' and i.day = '" + '-'.join([year,str(m),str(d)]) + "' and c.price != 0 and i.price != 0 order by percentage limit 5"
	mycursor.execute(query)
	myresult = mycursor.fetchall()
	message += "Top 5 Depreciations (Over 90 days): \n"
	for result in myresult:
		message += result[0] + ': ' + str(round(float(result[1]), 2)) + '%\n'
	message += '\n'	

	query = "select c.card_title, ((c.price/i.price)-1)*100 as percentage from card_inventories as c, card_inventories as i, ownership where c.card_id = i.card_id and c.day = '" + str(date.today()) + "' and i.day = '" + '-'.join([year,str(m),str(d)]) + "' and c.price != 0 and i.price != 0 and c.card_id = ownership.card_id and ownership.username = '" +user+ "' order by percentage desc limit 5"
	mycursor.execute(query)	
	myresult = mycursor.fetchall()
	message += "Top 5 Appreciations (Over 90 days): \n"
	for result in myresult:
		message += result[0] + ': ' + str(round(float(result[1]), 2)) + '%\n'
	message += '\n'	
	return message

def send_message(number, m):

	accId = 'ACcb808a16e5c07d4d167e1c153312989b'
	authToken = 'dfac35087a215ec4205eefb41e9a8b02'

	client = Client(accId, authToken)

	message = client.messages \
			  .create(
			     body = m,
				 from_ = '+12563776074',
				 to = number
			)
def get_current_users():

	users = {}
	
	mydb = mysql.connector.connect(
		host 	= "localhost",
		user 	= "ebianchi",
		passwd 	= "ebianchi",
		database= "ebianchi",
	)

	mycursor = mydb.cursor()

	mycursor.execute("select username, phonenumber from users")

	myresult = mycursor.fetchall()

	for result in myresult:
		users[result[0]] = result[1]
	
	return users

def send_updates(users):
	
	for user in users:	
		message = build_message(user)
		send_message(users[user],message)

if __name__ == '__main__':

	cardInfo = get_current_inventory()
	prodIds = get_product_ids(cardInfo)
	get_prices(prodIds, cardInfo)
	update_database()
	users = get_current_users()
	send_updates(users)
