<html>

	<head>
		<title>Register Here</title>	

		<link rel="stylesheet" href="./css/bootstrap.css">
        <link rel="stylesheet" href="./css/ible.css">
	</head>

	<body>
		</br>		
		<div class="container">
			<?php
				if(!empty($_GET['error'])){
					if (strcmp($_GET['error'], "1") == 0){
						echo'<div class="alert alert-warning" role="alert">Please enter matching passwords.</div>';
					}
					else if (strcmp($_GET['error'], "2") == 0){
						echo'<div class="alert alert-warning" role="alert">This username already exists. Please choose a new one.</div>';
					}
				}
				else{
					echo'<div class="alert alert-primary" role="alert">Please register below</div>';
				}
			?>
			<form action='create.php' method='get' onsubmit="return validateUser();">
                <div class="form-group">
                    <label for="usernameLabel">Username</label>
                    <input required name="username" type="username" class="form-control" id="usernameField" placeholder="Username">
                </div>
                <div class="form-group">
                    <label for="phoneLabel">Phone Number</label>
                    <input required name="phone" type="tel" class="form-control" id="phoneField" placeholder="Phone Number">
                </div>
                <div class="form-group">
                    <label for="passwordLabel">Password</label>
                    <input required name="password" type="password" class="form-control" id="passwordField" placeholder="Password">
                </div>
                <div class="form-group">
                    <label for="confirmPasswordLabel">Confirm Password</label>
                    <input required name="confirmPassword" type="password" class="form-control" id="confirmPasswordField" placeholder="Confirm Password">
                </div>
                <button type="submit" id="loginButton" class="btn btn-primary">
                    Login
                </button>
            </form>
			
			<a href="./login.php">Already have an account? Login Here</a>

		</div>
			
		<script src="./js/register.js"></script>

	</body>

</html>
