<?php
session_start(); 
$link = mysqli_connect('localhost', 'ebianchi', 'ebianchi') or die ('bleh');
mysqli_select_db($link, 'ebianchi');

$username = $_SESSION['user_id'];

if(isset($_GET['search'])){
	$sql = "select inventories.comic_id, comics.comic_title, issue_num, release_year, price, location, qty from inventories, comics where inventories.comic_id = comics.id and inventories.username = ? and comics.comic_title like ?";
	$stmt = $link->prepare($sql);
	$yo = '%' . $_GET['search'] . '%';
	$stmt->bind_param('ss', $_SESSION['user_id'], $yo);
	$stmt->execute();
	$stmt->bind_result($id, $title, $issue_num, $release_year, $price, $location, $qty);
}
else{
	$sql = "select inventories.comic_id, comics.comic_title, issue_num, release_year, price, location, qty from inventories, comics where inventories.comic_id = comics.id and inventories.username = ?";
	$stmt = $link->prepare($sql);
	$stmt->bind_param('s', $_SESSION['user_id']);
	$stmt->execute();
	$stmt->bind_result($id, $title, $issue_num, $release_year, $price, $location, $qty);
}


echo "<table class=\"table\" id=\"invTable\">\n";
echo "<thead>";
	echo "<tr>";
		echo "<th scope=\"col\">ID</th>";
		echo "<th scope=\"col\">Title</th>";
		echo "<th scope=\"col\">Issue Number</th>";
		echo "<th scope=\"col\">Release Year</th>";
		echo "<th scope=\"col\">Price</th>";
		echo "<th scope=\"col\">Location</th>";
		echo "<th scope=\"col\">Quantity</th>";
		echo "<th scope=\"col\">Add</th>";
		echo "<th scope=\"col\">Sub</th>";
		echo "<th scope=\"col\">Remove All</th>";
		echo "<th scope=\"col\">Edit</th>";
	echo "</tr>";
echo "</thead>";
echo "<tbody>";
while($stmt->fetch()){
	echo "\t<tr>\n";
	echo "\t<td>$id</td>\n";
	echo "\t<td id=\"titleid=$id\">$title</td>\n";
	echo "\t<td>$issue_num</td>\n";
	echo "\t<td>$release_year</td>\n";
	echo "\t<td id=\"price=$id\">$$price</td>\n";
	echo "\t<td id =\"loc=$id\">$location</td>\n";
	echo "\t<td id =\"qty=$id\">$qty</td>\n";
	echo "\t<td><span class=\"table-remove\"><button type=\"button\" onclick=\"add($id)\" class=\"btn green-button-color shadow\">+</button></span></td>\n";
	echo "\t<td><span class=\"table-remove\"><button type=\"button\" onclick=\"sub($id)\" class=\"btn yellow-button-color shadow\">-</button></span></td>\n";
	echo "\t<td><span class=\"table-remove\"><button type=\"button\" onclick=\"remove($id)\" class=\"btn red-button-color shadow\">Remove</button></span></td>\n";
  echo "\t<td><button id=\"$id\"type=\"button\" onclick=\"editComic($id)\"class=\"btn search-button green-button-color shadow\" data-toggle=\"modal\" data-target=\"#editComicModal\">Edit</button></td>\n";
	echo "\t</tr>\n";
}
echo "</tbody>";
echo "</table>";

$stmt->close();
?>
