<?php
session_start();
if(!isset($_SESSION['user_id'])){
    header("Location: http://db.cse.nd.edu/cse30246/ible/projectfiles/home.php?error=1");
}
?>

<!doctype html>
<html lang="en">
    <head>
        <title>Ible</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="Chandler Crane">
        <link rel="stylesheet" href="./css/bootstrap.css">
        <link rel="stylesheet" href="./css/simple-sidebar.css">
        <link rel="stylesheet" href="./css/ible.css">
        <link rel="icon" href="">
        <!-- Load Font Awesome Kit -->
        <script src="https://kit.fontawesome.com/63f61d4c7f.js"></script>
    </head>
    <body>
		<?php include 'session.php';?>
        <div class="d-flex" id="wrapper">
            <!-- Sidebar -->
            <div class="bg-light border-right" id="sidebar-wrapper">
                <div class="sidebar-heading main-title-color">Ible</div>
                <div class="list-group list-group-flush">
                <a id="dashboardButton" class="list-group-item list-group-item-action sidebar-button-color-notselected">Home</a>
                <a id="inventoryButton" class="list-group-item list-group-item-action sidebar-button-color-notselected">My Comics</a>
                <a id="cardButton" class="list-group-item list-group-item-action sidebar-button-color-notselected">My Cards</a>
                <a id="settingsButton" class="list-group-item list-group-item-action sidebar-button-color-notselected">Settings</a>
                </div>
            </div>
            <!-- /#sidebar-wrapper -->
       
            <!-- Page Content -->
            <div id="page-content-wrapper">
        
                <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
                    <button class="btn main-button-color shadow" id="menu-toggle">
                        <i class="fas fa-ellipsis-h fa-lg"></i>
                    </button>

                    <!-- <h3>Ible Dashboard</h3> -->
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse responsive-nav-grid" id="navbarSupportedContent">
                        <ul class="navbar-nav mt-2 mt-lg-0 center push-right-lg">
                            <li id="nameLabel" class="main-title-color"></li>
                            <li id="companyLabel" class="main-title-color"></li>
                        </ul>
                        <button action="logout.php" id="logoutButton" class="btn btn-outline-danger shadow">
                                Logout
                        </button>
                    </div>
                </nav>
               	
                <!-- Dashboard Tab -->
                <div id="dashboardSection" class="container-fluid page-content" style="display:none">
					<h1 class="mt-4 page-title main-title-color">Average Comic Prices</h1>
					<h5 class="helperText">Search for a comic below!</h5>
					<form action="home.php" method="get">
						<input type="hidden" name="px" value="1" />
						<div class="input-group">
							<input type="text" name="search">
							<span class="input-group-btn">
							<button type="submit" class="btn search-button main-button-color shadow">Submit</button>
							</span>
						</div>
					</form>

					<br>

					<?php include 'comicprices.php';?>

				</div>
                <!-- End -->

                <!-- Settings Tab -->
                <div id="settingsSection" class="container-fluid page-content" style="display:none">
                    <h1 class="mt-4 page-title main-title-color">Settings</h1>
                    <h5 class="helperText">If you need to change the information listed on this page, <a href="mailto:ccrane2@nd.edu">please send an email to Chandler</a></h5>
                    <form>
                        <div class="form-row">
                            <label for="exampleUsername" class="mt-4 settings-section-title title-light-blue">Username</label>
                            <input type="text" readonly class="form-control settings-input" id="usernameInput">
                        </div>
					</form>
					<form>
						<div class="form-row">
                            <label for="examplePassword" class="mt-4 settings-section-title title-light-blue">New Password</label>
                            <input required type="password" class="form-control settings-input" id="passwordInput">
                        </div>
                        <button type="submit" id="updatePasswordButton" class="btn main-button-color">Update Password</button>
					</form>
					<button id="deleteUserButton" class="btn btn-outline-danger">
							Delete User
					</button>
                </div>
                <!-- End -->


				<!-- Inventory Tab -->
                <div id="inventorySection" class="container-fluid page-content" style="display:none">
                    <h1 class="mt-4 page-title main-title-color">Comic Book Inventory</h1>
                    <h5 class="helperText">Here are your comics!</h5>
					<div class="search-mini-nav">
						<div class="left-mini-nav">
							<form action="home.php?px=1" method="get">
								<input type="hidden" name="px" value="1" />
								<div class="input-group">
									<input type="text" class="search-button" name="search">
									<span class="input-group-btn">
										<button id="searchComicButton" type="submit" class="btn search-button main-button-color shadow">Search</button>
										<button id="clearComicSearchButton" type="button" class="btn search-button red-button-color shadow">Clear Search</button>
									</span>
								</div>
							</form>
						</div>
						<div class="right-mini-nav">
							<button id="addComicPopupButton"type="button" class="btn search-button green-button-color shadow" data-toggle="modal" data-target="#addComicModal">Add Comic</button>
							

						</div>
					</div>

					<form action="upload_comics.php" method="post" enctype="multipart/form-data">
					<div class="fileInputBar">
						<div class="fileInputSection custom-file">
							<input type="file" class="custom-file-input" name="fileToUploadComic" id="fileToUploadComic">
							<label class="custom-file-label" for="uploadFilename">Upload CSV File</label>
						</div>
						<button id="uploadCSVButtonComic" name="submit" type="submit" class="btn search-button yellow-button-color shadow">Upload</button>
						
						<div class="right-mini-nav">
							<?php include 'comic_pricing.php';?>
						</div>
					</div>
					</form>
					<!-- Pop-up to Add to Comic Book Inventory -->

					<div class="modal fade" id="addComicModal" tabindex="-1" role="dialog" aria-labelledby="addComicModalTitle" aria-hidden="true">
						<div class="modal-dialog modal-dialog-centered" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title main-title-color" id="exampleModalLongTitle">Add Comic Book to Inventory</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body popup-body">
									<div class="form-row">
										<label for="exampleTitle" class="mt-4 top-of-section normal-section-title title-light-blue">Comic ID</label>
										<input type="text" class="form-control normal-input" id="addTitleInput">
									</div>
									<div class="form-row">
										<label for="examplePrice" class="mt-4 normal-section-title title-light-blue">Price</label>
										<input type="number" min="0.01" step="0.01" class="form-control normal-input" id="addPriceInput">
									</div>
									<div class="form-row">
										<label for="exampleLocation" class="mt-4 normal-section-title title-light-blue">Location</label>
										<input type="text" class="form-control normal-input" id="addLocationInput">
									</div>
									<div class="form-row">
										<label for="examplePrice" class="mt-4 normal-section-title title-light-blue">Quantity</label>
										<input type="number" min="0" step="1" class="form-control normal-input" id="addQuantityInput">
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn red-button-color" data-dismiss="modal">Cancel</button>
									<button id="addComicButton" type="button" class="btn main-button-color">Add Comic</button>
								</div>
							</div>
						</div>
					</div>

					<!-- End Modal Pop-up -->

					<!-- Pop-up to Edit to Comic Book in Inventory -->
					
					<div class="modal fade" id="editComicModal" tabindex="-1" role="dialog" aria-labelledby="editComicModalTitle" aria-hidden="true">
						<div class="modal-dialog modal-dialog-centered" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title main-title-color" id="exampleModalLongTitle">Edit Comic Book</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body popup-body">
									<div class="form-row">
										<label for="exampleEmail" class="mt-4 settings-section-title title-light-blue">Comic Title</label>
										<input type="text" readonly class="form-control settings-input" id="TitleInput">
									</div>
									<div class="form-row">
										<label for="examplePrice" class="mt-4 normal-section-title title-light-blue">Price</label>
										<input type="number" min="0.01" step="0.01" class="form-control normal-input" id="editPriceInput">
									</div>
									<div class="form-row">
										<label for="exampleLocation" class="mt-4 normal-section-title title-light-blue">Location</label>
										<input type="text" class="form-control normal-input" id="editLocationInput">
									</div>
									<div class="form-row">
										<label for="examplePrice" class="mt-4 normal-section-title title-light-blue">Quantity</label>
										<input type="number" min="0" step="1" class="form-control normal-input" id="editQuantityInput">
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn red-button-color" data-dismiss="modal">Cancel</button>
									<button id="editComicButton" type="button" class="btn main-button-color">Save</button>
								</div>
							</div>
						</div>
					</div>
					
					<!-- End Modal Pop-up -->

					<?php include 'inventory.php';?>

					<script type="text/javascript">
						function remove(id){
							console.log(id);
							var url = new URL('http://db.cse.nd.edu/cse30246/ible/projectfiles/deletecomic.php?');
							url.searchParams.set("del", id);
							var newUrl = url.href;
							console.log(newUrl);
							window.location.href = newUrl
						}
						function add(id){
							console.log(id);
							var url = new URL('http://db.cse.nd.edu/cse30246/ible/projectfiles/add.php?');
							url.searchParams.set("add", id);
							var newUrl = url.href;
							console.log(newUrl);
							window.location.href = newUrl
						}
						function sub(id){
							console.log(id);
							var url = new URL('http://db.cse.nd.edu/cse30246/ible/projectfiles/sub.php?');
							url.searchParams.set("sub", id);
							var newUrl = url.href;
							console.log(newUrl);
							window.location.href = newUrl
						}
					</script>

                </div>
				<!-- END -->

                <!-- Card Inv Tab -->
                <div id="cardSection" class="container-fluid page-content" style="display:none">
					<h1 class="mt-4 page-title main-title-color">Card Inventory</h1>
                    <h5 class="helperText">Here are your cards!</h5>
					<div class="search-mini-nav">
						<div class="left-mini-nav">
							<form action="home.php?px=1" method="get">
								<input type="hidden" name="px" value="2" />
								
								<div class="input-group">
									<input type="text" class="search-button" name="search2">
									<span class="input-group-btn">
										<button id="searchCardButton" type="submit" class="btn search-button main-button-color shadow">Search</button>
										<button id="clearCardSearchButton" type="button" class="btn search-button red-button-color shadow">Clear Search</button>
									</span>
								</div>
							</form>
						</div>
						<div class="right-mini-nav">
							<button id="addCardPopupButton" type="button" class="btn search-button green-button-color shadow" data-toggle="modal" data-target="#addCardModal">Add Card</button>
						</div>
					</div>


					<form action="upload_cards.php" method="post" enctype="multipart/form-data">
					<div class="fileInputBar">
						<div class="fileInputSection custom-file">
							<input type="file" class="custom-file-input" name="fileToUploadCard" id="fileToUploadCard">
							<label class="custom-file-label" for="uploadFilename">Upload CSV File</label>
						</div>
						<button id="uploadCSVButtonCard" name="submit" type="submit" class="btn search-button yellow-button-color shadow">Upload</button>
					
						<div class="right-mini-nav">
							<?php include 'card_pricing.php';?>
						</div>
						
					</div>
					</form>
					<?php include 'cards_inv.php';?>
					
					<!-- Pop-up to Add to Comic Book Inventory -->

					<div class="modal fade" id="addCardModal" tabindex="-1" role="dialog" aria-labelledby="addCardModalTitle" aria-hidden="true">
						<div class="modal-dialog modal-dialog-centered" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title main-title-color" id="exampleModalLongTitle">Add Card to Inventory</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body popup-body">
									<div class="form-row">
										<label for="exampleTitle" class="mt-4 top-of-section normal-section-title title-light-blue">Card ID</label>
										<input type="text" class="form-control normal-input" id="addIDInput">
									</div>
									<div class="form-row">
										<label for="examplePrice" class="mt-4 normal-section-title title-light-blue">Quantity</label>
										<input type="number" min="0" step="1" class="form-control normal-input" id="addQtyInput">
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn red-button-color" data-dismiss="modal">Cancel</button>
									<button id="addCardButton" type="button" class="btn main-button-color">Add Card</button>
								</div>
							</div>
						</div>
					</div>

					<!-- End Modal Pop-up -->
					
					<script type="text/javascript">
						function remove_card(id){
							console.log(id);
							var url = new URL('http://db.cse.nd.edu/cse30246/ible/projectfiles/deletecard.php?');
							url.searchParams.set("del", id);
							var newUrl = url.href;
							console.log(newUrl);
							window.location.href = newUrl
						}
						function add_card(id){
							console.log(id);
							var url = new URL('http://db.cse.nd.edu/cse30246/ible/projectfiles/add_card.php?');
							url.searchParams.set("add", id);
							var newUrl = url.href;
							console.log(newUrl);
							window.location.href = newUrl
						}
						function sub_card(id){
							console.log(id);
							var url = new URL('http://db.cse.nd.edu/cse30246/ible/projectfiles/sub_card.php?');
							url.searchParams.set("sub", id);
							var newUrl = url.href;
							console.log(newUrl);
							window.location.href = newUrl
						}
					</script>

				</div>
                <!-- End -->

            </div>
            <!-- /#page-content-wrapper -->
    
        </div>
        <!-- /#wrapper -->


        <!-- Bootstrap JS files -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>


		<!-- DB JS Files -->
		<script src="./js/addcomic.js"></script>
		<script src="./js/addcard.js"></script>
		<script src="./js/editcomic.js"></script>
        <script src="./js/upload.js"></script>

        <!-- Main JS file -->
        <script src="./js/main.js"></script>

        <!-- Menu Toggle Script -->
        
		<script>
            $("#menu-toggle").click(function(e) {
                e.preventDefault();
                $("#wrapper").toggleClass("toggled");
            });
        </script>

		<script>
			$(".custom-file-input").on("change", function() {
			  var fileName = $(this).val().split("\\").pop();
			  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
			});
		</script>
	
    </body>
</html>




